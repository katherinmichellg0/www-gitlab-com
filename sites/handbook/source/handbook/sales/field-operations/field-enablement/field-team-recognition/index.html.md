---
layout: handbook-page-toc
title: "Field Team Recognition Programs"
description: "Field Team Recognition Programs aim to recognize Field team members for significant contributions to team performance. These programs are a direct result of Sales engagement survey data and CRO leaders' commitment to fostering a culture of recognition"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Background 

In response to the FY22 Field Engagement Survey, CRO leaders committed to focusing on two main areas of opportunity: 
1. Growth & Development 
1. Recognition 

CRO Leaders partnered with the People group and Field Enablement on initiatives to increase recognition across the Field team in FY22 and beyond, taking team member feedback and ideas closely into account during the planning process. The result was the launch of two recognition-based initiatives in FY22: 
1. Quarterly Achiever's Chat 
1. #deal-gratitude Slack chanel 

### Why Recognition? 

Why is recognition so important? [Research shows](https://greatergood.berkeley.edu/article/item/how_gratitude_can_transform_your_workplace) that gratitude fundamentally alters our brain chemistry and primes us for more compassion and happiness.

When it comes to work, [Harvard Business Review](https://hbr.org/2020/10/use-gratitude-to-counter-stress-and-uncertainty) shares that gratitude can help improve coworker relationships, increase perseverance through difficult tasks, and make teams more collaborative. Specifically: 
1. Improved mental wellness
1. Resilience to stress 
1. Helps with team bonding
1. Positively impacts our ability to make hard decisions and express self-control
1. Increased retention 
   - 63% of employees who are recognized are very unlikely to look for a new job ([source](https://www.hrtechnologist.com/articles/rewards-and-recognition/employee-recognition-and-retention-statistics/#))
1. Increased trust 
   - 50% of employees believe being thanked by managers not only improved their relationship but also built trust with their managers/executives ([source](https://www.tinypulse.com/blog/sk-employee-recognition-stats))

It's important to note that these results rely on a steady gratitude practice over a longer period, which means **we must make recognition a daily practice for our teams**. Recognition was a key component of Module 4 of the [Field Manager Development Program](https://about.gitlab.com/handbook/sales/field-manager-development/) in FY22-Q3.  

## Field Quarterly Achiever's Chat 

The Quarterly Achiever's Chat recognizes Field team members who have excelled in a given quarter by inviting them to join the CEO and CRO in a congratulatory chat. The benefit is three-fold:
1. Top performers receive facetime and recognition directly at the executive level
1. Knowledge sharing of best practices that can replicated by other Field team members
1. Leaders/teams outside of the Field organization gain visibility into Sales momentum and key wins

Key features of this call include: 
1. 25-minute call
1. Held within first month of new quarter
1. CEO and CRO attendance required
1. Not recorded 

#### Selection Criteria - Q4-FY22 

In total, 12 team members from across the Field organization will be recognized. The categories by segment include: 
1. Enterprise Sales
   1. ENT Rookie Award
   1. Land with Vision Expand with Purpose Award
   1. Services Attach Success Award 
1. Customer Success
   1. Top Delivery Team Results
   1. SA Customer Results
   1. T(e)AM Expansion
1. Commercial Sales
   1. Q3 Comm Rookie Award
   1. R7 Results Delivery Award
1. Channel 
   1. Q3 Channel Rookie Award 
   1. Partner Collaboration Award
1. Alliances 
   1. Alliances Peak Results Award
1. Field Operations
   1. Field Operations MVP

See the detailed criteria in [this spreadsheet](https://docs.google.com/spreadsheets/d/1hSHyI-jZL_TwrhHeZILQWIktfFJXr_3jQ16E56thnEk/edit?usp=sharing). Considerations include: 
1. Categories are reevaluated by CRO leadership and Field Operations every quarter and changed if deemed necessary. 
1. A team member cannot win a Quarterly Achiever award twice within a fiscal year. 

#### Quarterly Achiever's Chat Process and Timeline
1. Quarterly Achievers finalized within the second week of the new quarter
1. Quarterly Achievers announced and recognized in the first WW Field Sales Call following [QBRs](/handbook/sales/qbrs/) (typically 3 weeks into the new quarter)
1. Quarterly Achievers Chat held within the first month of new quarter 
1. CEO and CRO to congratulate and inquire about key learnings from achievers on call
1. CEO and CRO to pull out 2-3 of the most impactful key learnings and share with the larger team in a Slack post (either #ceo or #cro) after the call
1. Achievers recognized in the next-available [Field Flash](/handbook/sales/field-communications/field-flash-newsletter/)

## #deal-gratitude Slack Channel 

It is up to managers to uphold a steady drumbeat of recognition for their teams. Field Managers consistently asked for a dedicated forum to recognize their team members publicly. 

As a result, we launched the #deal-gratitude channel as a forum for Field team members to provide recognition and share best-practices specifically related to sales deals, customer support/expansion and channel & alliances momentum in a public channel rather than in a specific team channel or meeting. 

Key features of this channel include: 
1. Public channel
1. Managers encouraged to post regularly, but all team members are able to post
1. Reports sent to Field Managers on a monthly basis regarding activity in the channel with reminders to contribute to the channel

#### Best Practices for Managers+ 
When using this channel, Field managers+ should keep these best practices in mind: 
1. **Take the lead.** When your team member has a big win, let them know that you will be the one to post a congratulatory message because you also want to recognize *them*. Asking for their help to gather all names and details is okay, but remember to proactively take the lead on giving your team member recognition, in addition to all of the team members who helped them.  
1. **Be inclusive.** It takes a team to succeed – tag all team members who played an important role in the win/behavior that you are recognizing.  
1. **Give recognition often.** Don't wait until the end of the quarter to recognize your team members – provide recognition as the activity happens, and provide it often. 
1. **Share best practices.** Use this opportunity to show the Field team "what good looks like." Share all the detail and context (resources, etc.) that you can to allow other team members to learn from and repeat successful, recognition-worthy behavior.  

#### What is the difference between #deal-gratitude and #thanks? 
The #thanks is a channel for any team member to thank anyone for contributions in any aspect of their job or personal development. #deal-gratitude is a channel specifically dedicated to recognizing team members for outstanding efforts specifically related to sales deals, customer support/expansion and channel & alliances momentum. While this might include some "thanks," it is not limited to it. Posts in this channel should also share best practices and lessons learned so that the outstanding behaviors recognized are repeatable by peers.

**When to use #deal-gratitude**

1. Manager+ recognizing a rep and the full account team (including team members outside of Sales) for going above and beyond to close a customer deal. Shares insights into how/why we won deal.
1. Manager+ or team member commending an account team for tenacious efforts to close a deal even though it was lost. Shares insights into lost reasons.
1. Manager+ or team member commending a CS team member for positive feedback shared by a customer, saving an account from churn, etc.
1. Manager+ or team member recognizing a rep or team for successfully attaching professional services.
1. Manager+ or team member recognizing the Channel team for helping close a deal.
1. Manager+ or team member sharing a pitch deck or similar resource that helped close a deal/drive a crucial customer meeting.

**When to use #thanks**

1. Giving thanks exclusively to team members outside of Sales (i.e. team member wants to thank Product for releasing a meaningful feature that is crucial in a deal).
1. General thanks around coffee chats, meetings, answered questions, assisting fellow team members, troubleshooting, manager appreciation, etc.

### Additional Resources for Recognition

You can find more guidance on recognition and what is "recognition worthy" at GitLab [in this handbook page](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/#recognition).

Some additional resources include: 
1. [The Positive Power of Gratitude for Remote Teams](https://www.heykona.com/post/the-positive-power-of-gratitude-for-remote-teams) - Kona Blog 


